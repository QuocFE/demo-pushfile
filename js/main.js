$(document).ready(()=>{
    let status="off";
    $(".header__toggle").click(function(){
        $(".imageAll__item").toggleClass("imageItem--gap");
        $(".imageAll").toggleClass("imageAll--gap");
        if(status==="off"){
              $(this).css({ "box-shadow": "0px 0px 0px 0px #2e2e2e",
                    "transform": "translateY(4px)"});
            status="on";
        }else {
            $(this).css({"box-shadow": "0px 4px 0px 0px #2e2e2e",
            "transform": "translateY(0)"});
            status="off";
        }
    })
    $(".header__download").focusin(function(){
           $(this).css({ "box-shadow": "0px 0px 0px 0px #2e2e2e",
            "transform": "translateY(4px)"})
            let value= $(".progress__present").data("value");
                $(".progress__present").css({
                    "width":`${value}%`
                })
    })
    $(".header__download").blur(function(){
        $(this).css({ "box-shadow": "0px 4px 0px 0px #2e2e2e",
        "transform": "translateY(0)"})
        $(".progress__present").css({
            "width":`0`
        })
    })
   
    $(".navBarFullScreen__closedIcon").click(()=>{
        $(".navBarFullScreen").removeClass("navBarFullScreen--hide");
    })
    $(".header__bar").click(()=>{
       
        $(".navBarFullScreen").addClass("navBarFullScreen--hide");
    })
})